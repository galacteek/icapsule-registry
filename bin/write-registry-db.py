#!/usr/bin/env python

import sys
import os
import argparse
from omegaconf import OmegaConf
import requests


parser = argparse.ArgumentParser()
parser.add_argument('--src',
                    dest='reg',
                    default='registry-src.yaml')
parser.add_argument('--dst', dest='dest',
                    default='icapsule-registry.yaml')
args = parser.parse_args()

try:
    r = OmegaConf.load(args.reg)
    c = OmegaConf.to_container(r, resolve=True)

    for uri, conf in c['icapsules'].items():
        durl = conf.get('deploymentsUrl', None)
        if not durl:
            continue

        try:
            data = requests.request('GET', durl)

            if data.content:
                obj = OmegaConf.create(data.content.decode())

                c['icapsules'][uri]['deployments'] = \
                        OmegaConf.to_container(obj, resolve=True)
        except Exception:
            continue

    OmegaConf.save(c, args.dest)
except Exception as err:
    print(str(err))
    sys.exit(1)


sys.exit(0)
